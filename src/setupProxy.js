const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function (app) {
  app.use(
    '/api/check',
    createProxyMiddleware({
      target: 'https://www.bb.org.bd/investfacility/prizebond/searchPbond.php',
      changeOrigin: true,
    })
  )

  app.use(
    '/api/images',
    createProxyMiddleware({
      target: 'https://www.bb.org.bd/investfacility/prizebond/',
      changeOrigin: true,
      pathRewrite: {
        '^/api/images': '/images',
      },
    })
  )
}
