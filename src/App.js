import { Button, Container, Heading, Stack, Textarea } from "@chakra-ui/core";
import React, { useCallback, useRef, useState } from "react";
import { getFallbackUrl } from "./utils/api";

const rangeSeparatorPattern = / *(?:~|-|to) */;

const numberPattern = /^[0-9]+$/;

const parseValue = (value) => {
  return value
    .split(/(?:[,;\n]+|and|or)/)
    .filter(Boolean)
    .reduce((items, part) => {
      if (rangeSeparatorPattern.test(part)) {
        const [start, end] = part
          .trim()
          .split(rangeSeparatorPattern)
          .map((item) => item.trim());

        if (!numberPattern.test(start)) {
          throw new Error(`Invalid value: ${start}`);
        }

        if (!numberPattern.test(end)) {
          throw new Error(`Invalid value: ${end}`);
        }

        items.push(`${start}~${end}`);
      } else {
        const item = part.trim();

        if (!numberPattern.test(item)) {
          throw new Error(`Invalid value: ${item}`);
        }

        items.push(item);
      }
      return items;
    }, [])
    .join(",");
};

const placeholder = `
12345670
12345675 and 12345678
12345680 to 12345700
12345900 - 12345950
12345960 ~ 12345965
`.trim();

function App() {
  const textRef = useRef(null);
  const [value, setValue] = useState(() => {
    const value = localStorage.getItem("lastValue");
    return value || "";
  });

  const onClick = useCallback(() => {
    const lastValue = localStorage.getItem("lastValue");
    const value = textRef.current.value.trim();
    if (lastValue !== value) {
      localStorage.setItem("lastValue", value);
      setValue(value);
    }

    window.open(getFallbackUrl(parseValue(value)));
  }, []);

  return (
    <Container maxW="lg" centerContent mt="2rem">
      <Stack spacing="1rem">
        <Heading as="h3">Bangladesh Bank Prize Bond Checker</Heading>

        <Textarea
          ref={textRef}
          defaultValue={value}
          rows="5"
          size="lg"
          resize="vertical"
          placeholder={placeholder}
        />

        <Button size="lg" colorScheme="blue" onClick={onClick}>
          GO Check!
        </Button>
      </Stack>
    </Container>
  );
}

export default App;
