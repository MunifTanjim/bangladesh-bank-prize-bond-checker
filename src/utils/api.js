const endpoint = `/api/check`;

function tableToJson(table) {
  const data = [];

  const headers = [];
  for (let i = 0; i < table.rows[0].cells.length; i++) {
    headers[i] = table.rows[0].cells[i].innerHTML
      .toLowerCase()
      .replace(/ /gi, "");
  }

  for (let i = 1; i < table.rows.length; i++) {
    const tableRow = table.rows[i];
    const rowData = {};

    for (let j = 0; j < tableRow.cells.length; j++) {
      rowData[headers[j]] = tableRow.cells[j].innerHTML;
    }

    data.push(rowData);
  }

  return data;
}

export const getFallbackUrl = (txtNumbers) => {
  return `https://www.bb.org.bd/investfacility/prizebond/searchPbond.php?txtNumbers=${txtNumbers}`;
};

export const getPrizeBondMatches = async (txtNumbers) => {
  const response = await fetch(`${endpoint}?txtNumbers=${txtNumbers}`);

  if (!response.ok) {
    return {
      ok: false,
      status: response.status,
      statusText: response.statusText,
      message: `Something Went Wrong!`,
      fallbackUrl: getFallbackUrl(txtNumbers),
    };
  }

  const htmlString = await response.text();
  const parser = new DOMParser();
  const doc = parser.parseFromString(htmlString, "text/html");
  const table = doc.getElementsByClassName("tableData")[0];
  const items = table ? tableToJson(table) : [];

  return {
    ok: true,
    status: response.status,
    statusText: response.statusText,
    items,
  };
};
